package br.com.wiretecnologia.gateway.spartacus.utils;


import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import java.util.Objects;

public class EmailUtils {

    public static final String VARIAVEL_TEMPLATE = "CONTEUDO";
    public static final String TEMPLATE_EMAIL = "templates/relatorio_template.html";

    private EmailUtils() {
        //
    }

    private static TemplateEngine getHtmlTemplate() {
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode("HTML");
        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);
        return templateEngine;
    }

    public static String addSignatureToContent(String conteudoEmail, String templateName) {
        TemplateEngine templateEngine = getHtmlTemplate();
        Context context = new Context();
        context.setVariable(VARIAVEL_TEMPLATE, conteudoEmail);

        if (Objects.isNull(templateName) || templateName.isEmpty()) {
            templateName = TEMPLATE_EMAIL;
        }

        return templateEngine.process(templateName, context);
    }

    public static String addSignatureToContent(String conteudoEmail) {
        return addSignatureToContent(conteudoEmail, "");
    }
}
