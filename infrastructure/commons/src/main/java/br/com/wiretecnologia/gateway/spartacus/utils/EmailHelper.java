package br.com.wiretecnologia.gateway.spartacus.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

public class EmailHelper {

    private final MimeMessageHelper helper;

    public EmailHelper(MimeMessageHelper helper) {
        this.helper = helper;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailHelper.class);

    public void setFrom(String from) throws MessagingException {
        helper.setFrom(from);
    }

    public void setFrom(String from, String personal) {
        try {
            helper.setFrom(from, personal);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }


    public void addTo(String to) {
        try {
            helper.addTo(to);
        } catch (MessagingException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public void addCc(String cc) {
        try {
            helper.addCc(cc);
        } catch (MessagingException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public void addBcc(String bcc) {
        try {
            helper.addBcc(bcc);
        } catch (MessagingException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public void setSubject(String subject) {
        try {
            helper.setSubject(subject);
        } catch (MessagingException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public void setBody(String body, boolean html) {
        try {
            helper.setText(body, html);
        } catch (MessagingException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public void setBodyAsHtml(String body) {
        try {
            helper.getMimeMessage().setContent(body, "text/html; charset=utf-8");
        } catch (MessagingException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }


    public void addAttachment(File file) {
        try {
            DataSource dataSource = new FileDataSource(file);
            helper.addAttachment(file.getName(), dataSource);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public MimeMessage getMessage() {
        return helper.getMimeMessage();
    }
}
