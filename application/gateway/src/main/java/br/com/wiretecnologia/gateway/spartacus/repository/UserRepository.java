package br.com.wiretecnologia.gateway.spartacus.repository;


import br.com.wiretecnologia.gateway.spartacus.model.PersonProfileEntity;
import br.com.wiretecnologia.gateway.spartacus.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends JpaRepository<UserEntity,Long> {

    UserEntity findByUserNameAndStatus(String userName, String status);

    UserEntity findByUserName(String userName);

    UserEntity findByPersonProfileEntity(PersonProfileEntity personProfileEntity);

//    UserEntity findBy(PersonProfileEntity personProfileEntity);


}
