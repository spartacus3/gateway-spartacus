package br.com.wiretecnologia.gateway.spartacus.controller;

import br.com.wiretecnologia.gateway.spartacus.model.PersonProfileEntity;
import br.com.wiretecnologia.gateway.spartacus.model.UserEntity;
import br.com.wiretecnologia.gateway.spartacus.service.EmailService;
import br.com.wiretecnologia.gateway.spartacus.service.PersonProfileService;
import br.com.wiretecnologia.gateway.spartacus.service.PersonProfileServiceImpl;
import br.com.wiretecnologia.gateway.spartacus.service.UserServiceImpl;
import br.com.wiretecnologia.gateway.spartacus.util.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Objects;

@RestController
@RequestMapping("/api/v1/login")
@Slf4j
public class LoginController {

    private final UserServiceImpl userServiceImpl;
    private final EmailService emailService;
    private final PersonProfileServiceImpl personProfileServiceImpl;

    @Autowired
    public LoginController(UserServiceImpl userServiceImpl, EmailService emailService, PersonProfileService personProfileService, PersonProfileServiceImpl personProfileServiceImpl) {
        this.userServiceImpl = userServiceImpl;
        this.emailService = emailService;
        this.personProfileServiceImpl = personProfileServiceImpl;
    }

    @PostMapping(path = "/recuperarsenha", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CommonResponse> recuperarSenha(@RequestParam String email) {
        log.info("# Usando fluxo de alterar senha.");
        PersonProfileEntity personProfileEntity = personProfileServiceImpl.findbyEmail(email);
        if (Objects.nonNull(personProfileEntity)) {
            log.info("# Usuario existente na base");
            UserEntity userEntity = userServiceImpl.findByPersonProfileId(personProfileEntity);
            String passwordAlterado = userServiceImpl.generateNewPassaword(userEntity);
            emailService.fluxoEmailRecuperarSenha(passwordAlterado, userEntity.getPersonProfileEntity().getEmail());
            return new ResponseEntity<>(new CommonResponse("Nova senha gerada com sucesso. Verifique seu email.", true, null, HttpStatus.OK.value()), HttpStatus.ACCEPTED);
        } else {
            log.info("# Usuario não existe na base de dados");
            return new ResponseEntity<>(new CommonResponse("Usuario não existe na base de dados.", true, null, HttpStatus.NOT_FOUND.value()), HttpStatus.ACCEPTED);
        }
    }

    @GetMapping(path = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CommonResponse> userLogout(@RequestParam String userName, Principal principal) {
        log.info("Request Received for userLogout() ...");
        return new ResponseEntity<>(new CommonResponse("User Logout Successful.",true,null,HttpStatus.OK.value()), HttpStatus.ACCEPTED);
    }

}
