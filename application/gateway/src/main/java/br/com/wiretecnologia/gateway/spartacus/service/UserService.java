package br.com.wiretecnologia.gateway.spartacus.service;


import br.com.wiretecnologia.gateway.spartacus.dto.UserSignupDTO;
import br.com.wiretecnologia.gateway.spartacus.model.UserEntity;

/**
 * @Author : Amran Hosssain on 6/23/2020
 */
public interface UserService {

    String signup(UserSignupDTO userSignupDTO);

    UserEntity findByUserName(String userName);
}
