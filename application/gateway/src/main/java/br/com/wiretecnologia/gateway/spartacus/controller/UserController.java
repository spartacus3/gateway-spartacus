package br.com.wiretecnologia.gateway.spartacus.controller;


import br.com.wiretecnologia.gateway.spartacus.dto.UserSignupDTO;
import br.com.wiretecnologia.gateway.spartacus.service.UserService;
import br.com.wiretecnologia.gateway.spartacus.util.CommonResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.Serializable;


@RestController
@RequestMapping("/api/v1/user")
public class UserController implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CommonResponse> userSignup(@Valid @RequestBody UserSignupDTO userSignupDTO) {
        LOGGER.info("Request Received for userSignup() ...");
        String result = userService.signup(userSignupDTO);
        if (result.equalsIgnoreCase(HttpStatus.OK.name())) {
            return new ResponseEntity<>(new CommonResponse("Cadastrado com Successo", true, null, HttpStatus.CREATED.value()), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(new CommonResponse("Erro ao cadastrar. Por favor entre em contato com o suporte.", true, null, HttpStatus.EXPECTATION_FAILED.value()), HttpStatus.EXPECTATION_FAILED);
    }
}
