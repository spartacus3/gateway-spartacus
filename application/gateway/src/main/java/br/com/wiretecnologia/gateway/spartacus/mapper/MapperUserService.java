package br.com.wiretecnologia.gateway.spartacus.mapper;


import br.com.wiretecnologia.gateway.spartacus.constraint.UserStatus;
import br.com.wiretecnologia.gateway.spartacus.dto.UserSignupDTO;
import br.com.wiretecnologia.gateway.spartacus.model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.time.LocalDateTime;

@Component
public class MapperUserService implements Serializable {

    @Autowired private PasswordEncoder passwordEncoder;

    public UserEntity mapUserFromDTO(UserSignupDTO userSignupDTO) {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName(userSignupDTO.getUserName());
        userEntity.setStatus(UserStatus.ACTIVE.getStatus());
        userEntity.setPassword(passwordEncoder.encode(userSignupDTO.getPassword()));
        userEntity.setCreateDate(LocalDateTime.now());
        return userEntity;
    }

}
