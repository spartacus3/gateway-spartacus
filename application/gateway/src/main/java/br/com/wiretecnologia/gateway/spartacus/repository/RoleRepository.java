package br.com.wiretecnologia.gateway.spartacus.repository;

import br.com.wiretecnologia.gateway.spartacus.model.UserRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<UserRoleEntity, Long> {

    UserRoleEntity findByRoleId(Long id);
}
