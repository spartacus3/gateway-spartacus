package br.com.wiretecnologia.gateway.spartacus.service;

import br.com.wiretecnologia.gateway.spartacus.constraint.UserStatus;
import br.com.wiretecnologia.gateway.spartacus.dto.UserSignupDTO;
import br.com.wiretecnologia.gateway.spartacus.mapper.MapperUserService;
import br.com.wiretecnologia.gateway.spartacus.model.PersonProfileEntity;
import br.com.wiretecnologia.gateway.spartacus.model.UserEntity;
import br.com.wiretecnologia.gateway.spartacus.model.UserRoleEntity;
import br.com.wiretecnologia.gateway.spartacus.repository.UserRepository;
import br.com.wiretecnologia.gateway.spartacus.util.UserDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.stereotype.Service;

import java.util.UUID;


@Service(value = "userService")
public class UserServiceImpl implements UserService, UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MapperUserService mapperUserService;
    @Autowired
    private RoleService roleService;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        LOGGER.info("loadUserByUsername() method call...");
        boolean isAccountNonLocked = true;
        UserEntity userEntity = userRepository.findByUserNameAndStatus(userName, UserStatus.ACTIVE.getStatus());
        if (userEntity == null) {
            LOGGER.error("User not found or user deactivated.");
            throw new RuntimeException("User not found or account deactivated.");
        }
//        Boolean isNonVerified = customerEmailVerificationRepository.existsByUserNameAndCustomer_CustomeridAndEmailVerification(user.getUserName(), user.getCustomer().getCustomerid(), 0);
//        if (isNonVerified) {
//            LOGGER.error("Sign up complete but, Email is not verified");
//            throw new RuntimeException("Sign up complete but, Email is not verified");
//        }
        //This section for account lock or unlocked.
//        Integer result = loginAttemptAuditService.countLoginAttmptByDate(user.getUserName());
//        if (result != null) {
//            if (result >= invalidLoginAttempt) {
//                isAccountNonLocked = false;
//            }
//        } else {
//            System.out.println("Some thing wrong in user information.");
//        }
        return new UserDetail(userEntity, isAccountNonLocked);
    }

    @Override
    public String signup(UserSignupDTO userSignupDTO) {
        LOGGER.info("Cadastrando novo user method call.");
        try {

            //Find Patient ROLE
//            UserRoleEntity roleEntity = roleService.getAllRoles()
//                    .stream()
//                    .filter(r -> r.getRoleName().equalsIgnoreCase(UserStatus.PATIENT.getStatus()))
//                    .findAny()
//                    .orElse(null);
            UserRoleEntity roleEntity = roleService.getAllRoles().stream().findAny().orElse(null);

            if (null == roleEntity || roleEntity.getStatus().equalsIgnoreCase(UserStatus.INACTIVE.getStatus())) {
                throw new RuntimeException("Signup Request can't process");
            }
            //Finally User Signup
            UserEntity userEntity = mapperUserService.mapUserFromDTO(userSignupDTO);
            userEntity.setUserRoleEntity(roleEntity);
            UserEntity userAfterSave = userRepository.save(userEntity);
            if (userAfterSave != null && userAfterSave.getUserId() != null) {
                return HttpStatus.OK.name();
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            return HttpStatus.EXPECTATION_FAILED.name();
        }
        return null;
    }

    @Override
    public UserEntity findByUserName(String userName) {
        LOGGER.info("signup() method call...");
        return userRepository.findByUserName(userName);
    }

    public UserEntity findByPersonProfileId(PersonProfileEntity personProfileEntity) {
        LOGGER.info("signup() method call...");
        return userRepository.findByPersonProfileEntity(personProfileEntity);
    }

    public String generateNewPassaword(UserEntity userEntity) {
        LOGGER.info("# Gerando novo password");

        UUID uuid = UUID.randomUUID();
        String uuidPassword = uuid.toString().replace("-", "").substring(0, 8);
        String passwordEncript = PasswordEncoderFactories.createDelegatingPasswordEncoder().encode(uuidPassword);
        LOGGER.info("Password: {}", uuidPassword);
        userEntity.setPassword(passwordEncript);
        userRepository.save(userEntity);
        LOGGER.info("# Password salvo com sucesso.");
        return uuidPassword;
    }
}
