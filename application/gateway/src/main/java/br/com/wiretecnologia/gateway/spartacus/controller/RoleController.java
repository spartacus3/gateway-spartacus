package br.com.wiretecnologia.gateway.spartacus.controller;


import br.com.wiretecnologia.gateway.spartacus.constraint.UserStatus;
import br.com.wiretecnologia.gateway.spartacus.model.UserRoleEntity;
import br.com.wiretecnologia.gateway.spartacus.service.RoleServiceImpl;
import br.com.wiretecnologia.gateway.spartacus.util.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/v1/roller")
@Slf4j
public class RoleController {

    private final RoleServiceImpl roleServiceImpl;

    @Autowired
    public RoleController(RoleServiceImpl roleServiceImpl) {
        this.roleServiceImpl = roleServiceImpl;
    }

    @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CommonResponse> getRoles() {
        List<UserRoleEntity> roles = roleServiceImpl.getAllRoles();
        if (roles.isEmpty()) {
            return new ResponseEntity<>(new CommonResponse("", true, roles, HttpStatus.OK.value()), HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(new CommonResponse("Não existe roles na base de dados.", true, null, HttpStatus.NOT_FOUND.value()), HttpStatus.ACCEPTED);

        }
    }

    @GetMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CommonResponse> getRoleID(@PathVariable("id") Long id) {
        UserRoleEntity roleBuscada = roleServiceImpl.getRoleId(id);

        if (Objects.isNull(roleBuscada)) {
            return new ResponseEntity<>(new CommonResponse("Role não encontrado.", true, null, HttpStatus.NOT_FOUND.value()), HttpStatus.ACCEPTED);

        } else {
            return new ResponseEntity<>(new CommonResponse("", true, roleBuscada, HttpStatus.OK.value()), HttpStatus.ACCEPTED);
        }
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CommonResponse> cadastrarRole(@Valid @RequestBody UserRoleEntity userRoleEntity) {
        UserRoleEntity newRole = roleServiceImpl.save(userRoleEntity);
        return new ResponseEntity<>(new CommonResponse("Cadastrado com sucesso.", true, newRole, HttpStatus.OK.value()), HttpStatus.ACCEPTED);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CommonResponse> getRoleID(@Valid @RequestBody UserRoleEntity userRoleEntity) {
        UserRoleEntity roleBuscada = roleServiceImpl.getRoleId(userRoleEntity.getRoleId());

        if (Objects.isNull(roleBuscada)) {
            return new ResponseEntity<>(new CommonResponse("Role não encontrado.", true, null, HttpStatus.NOT_FOUND.value()), HttpStatus.ACCEPTED);

        } else {
            roleServiceImpl.save(userRoleEntity);
            return new ResponseEntity<>(new CommonResponse("", true, userRoleEntity, HttpStatus.OK.value()), HttpStatus.ACCEPTED);
        }
    }

    @DeleteMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CommonResponse> cadastrarRole(@PathVariable("id") Long id) {
        UserRoleEntity roleBuscada = roleServiceImpl.getRoleId(id);
        if (Objects.isNull(roleBuscada)) {
            return new ResponseEntity<>(new CommonResponse("Role não encontrado.", true, null, HttpStatus.NOT_FOUND.value()), HttpStatus.ACCEPTED);

        } else {
            roleBuscada.setStatus(UserStatus.INACTIVE.getStatus());
            roleServiceImpl.save(roleBuscada);
            return new ResponseEntity<>(new CommonResponse("", true, roleBuscada, HttpStatus.OK.value()), HttpStatus.ACCEPTED);
        }
    }


}
