package br.com.wiretecnologia.gateway.spartacus.service;

import br.com.wiretecnologia.gateway.spartacus.model.UserRoleEntity;

import java.util.List;


/**
 * @Author : Amran Hosssain on 6/23/2020
 */
public interface RoleService {

    List<UserRoleEntity> getAllRoles();

}
