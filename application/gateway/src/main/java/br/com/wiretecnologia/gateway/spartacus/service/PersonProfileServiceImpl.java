package br.com.wiretecnologia.gateway.spartacus.service;

import br.com.wiretecnologia.gateway.spartacus.model.PersonProfileEntity;
import br.com.wiretecnologia.gateway.spartacus.repository.PersonProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PersonProfileServiceImpl implements PersonProfileService {

    @Autowired
    private PersonProfileRepository personProfileRepository;

    public PersonProfileEntity findbyEmail(String email) {
        return personProfileRepository.findByEmail(email);
    }
}
