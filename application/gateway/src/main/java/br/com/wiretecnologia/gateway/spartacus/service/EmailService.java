package br.com.wiretecnologia.gateway.spartacus.service;


import br.com.wiretecnologia.gateway.spartacus.conts.ConstsGateway;
import br.com.wiretecnologia.gateway.spartacus.utils.EmailHelper;
import br.com.wiretecnologia.gateway.spartacus.utils.EmailUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class EmailService {


    private final JavaMailSender javaMailSender;

    @Autowired
    public EmailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }


    public void fluxoEmailRecuperarSenha(String newPassword, String destinatario) {
        try {

            log.info("# Enviado e-mail com nova senha");
            List<String> destinatarios = Arrays.asList(destinatario);
            String mensagem = String.format(ConstsGateway.CONTEUDO_EMAIL_RELATORIO, newPassword);

            log.info("# Enviando e-mail");
            this.enviarEmail(ConstsGateway.ASSUNTO_EMAIL_RECUPERACAO, destinatarios, mensagem);
            log.info("# E-mail enviado com sucesso");

            log.info(ConstsGateway.SEPARATOR_REPEAT);

        } catch (Exception e) {
            log.error(ConstsGateway.ERRO_EXCEPTION, e);
            log.info(ConstsGateway.SEPARATOR_REPEAT);
        }
    }

    private EmailHelper createMessage() {
        MimeMessage message = javaMailSender.createMimeMessage();
        try {
            return new EmailHelper(new MimeMessageHelper(message, true));
        } catch (MessagingException e) {
            return null;
        }
    }

    public void enviarEmail(String assunto, List<String> destinatarios, String conteudo) throws Exception {
        this.enviarEmail(assunto, conteudo, destinatarios, null, "");
    }

    public void enviarEmail(String assunto, List<String> destinatarios, String conteudo, List<File> attachments) throws Exception {
        this.enviarEmail(assunto, conteudo, destinatarios, attachments, "");
    }

    public void enviarEmail(String assunto, String conteudo, List<String> destinatario, List<File> attachments, String template) throws Exception {
        EmailHelper message = this.createMessage();

        if (Objects.isNull(message)) {
            throw new Exception("Falha ao criar mensagem de e-mail");
        }

        message.setFrom(ConstsGateway.EMAIL_MONITORAMENTO, ConstsGateway.PERSONAL_EMAIL_SPARTACUS);
        destinatario.forEach(message::addTo);
        message.setSubject(assunto);

        conteudo = EmailUtils.addSignatureToContent(conteudo, template);

        message.setBody(conteudo, true);

        if (Objects.nonNull(attachments)) {
            for (File attachament : attachments) {
                message.addAttachment(attachament);
            }
        }

        this.send(message.getMessage());
    }

    private void send(MimeMessage message) {
        javaMailSender.send(message);
    }
}
