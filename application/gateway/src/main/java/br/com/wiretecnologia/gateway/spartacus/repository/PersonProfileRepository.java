package br.com.wiretecnologia.gateway.spartacus.repository;


import br.com.wiretecnologia.gateway.spartacus.model.PersonProfileEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PersonProfileRepository extends JpaRepository<PersonProfileEntity,Long> {

    PersonProfileEntity findByEmail(String userName);
}
