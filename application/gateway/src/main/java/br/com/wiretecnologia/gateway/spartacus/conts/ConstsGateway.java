package br.com.wiretecnologia.gateway.spartacus.conts;

public class ConstsGateway {

    public static final String SEPARATOR_REPEAT = "----------------------------------------";
    public static final String TIME_ZONE = "America/Sao_Paulo";
    public static final String ERRO_MESSAGE = "# Erro - message: {}";
    public static final String ERRO_EXCEPTION = "# Erro - message:";

    // E-mail
    public static final String TEMPLATE_EMAIL = "templates/relatorio_template.html";
    public static final String TEMPLATE_EMAIL_ERRO = "templates/erro_template.html";
    public static final String VARIAVEL_TEMPLATE = "CONTEUDO";
    public static final String PERSONAL_EMAIL_SPARTACUS = "Spartacus - Wire Tecnologia";
    public static final String EMAIL_MONITORAMENTO = "monitoramentorobo@finchsolucoes.com.br";
    public static final String ASSUNTO_EMAIL_RECUPERACAO = "[Recuperação Senha - SPARTACUS]";
    public static final String ASSUNTO_EMAIL_RELATORIO_BACEN = "[Robô BACEN SISBAJUD] Relatorio execução %s";
    public static final String ASSUNTO_EMAIL_RELATORIO_CCS0012 = "[Robô BACEN SISBAJUD] Protocolados %s";
    public static final String CONTEUDO_EMAIL_RELATORIO = "Prezada(o),<br><br>Segue a nova senha solicitada: %s  <br><br>E-mail enviado automaticamente..";
    public static final String RETORNO_DIFERENTE_DE_200 = "Retorno diferente de 200";
    public static final String NOME_PADRAO_CCSU0012 = "CCSU0012_%s.txt";
}
