package br.com.wiretecnologia.gateway.spartacus.service;


import br.com.wiretecnologia.gateway.spartacus.model.UserRoleEntity;
import br.com.wiretecnologia.gateway.spartacus.repository.RoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Slf4j
@Service
public class RoleServiceImpl implements RoleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RoleServiceImpl.class);

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public List<UserRoleEntity> getAllRoles() {
        LOGGER.info("getAllRoles method call...");
        return roleRepository.findAll();
    }

    public UserRoleEntity getRoleId(Long id) {
        return roleRepository.findByRoleId(id);
    }

    public UserRoleEntity save(UserRoleEntity userRoleEntity) {
        return roleRepository.save(userRoleEntity);
    }
}
